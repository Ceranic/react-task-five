import React, {useState,useEffect} from 'react';
import './App.css';
import './components/WelcomeComponent.css'
import './components/LoginComponent.css'
import './components/RegisterComponent.css'
import {
  BrowserRouter,
  Switch,
  Route,
  Redirect
} from 'react-router-dom';
import RegisterComponent from './components/RegisterComponent'
import WelcomeComponent from './components/WelcomeComponent';
import LoginComponent from './components/LoginComponent';
import HomeComponent from './components/HomeComponent';
import ProfileComponent from './components/ProfileComponent';
import UserComponent from './components/UserComponent';
import AdminComponent from './components/AdminComponent';



function App(props) {

  localStorage.setItem('admin-user', JSON.stringify({
    username: 'admin',
    password: 'admin',
    email: 'admin@admin.com'
  }));




  const [users, setUsers] = useState(JSON.parse(localStorage.getItem('users') || '[]'));
  const [adminUser, setAdminUser] = useState(JSON.parse(localStorage.getItem('admin-user')))
  const [currentUser, setCurrentUser] = useState(JSON.parse(localStorage.getItem('current-user') || "{\"username\": \"\",\"password\": \"\",\"email\": \"\"}"));

  useEffect(() => {
    localStorage.setItem('users', JSON.stringify(users));
    console.log('Users Updated!');  
  },[users]);

  useEffect(() => {
    console.log('Current User: ', currentUser);
    localStorage.setItem('current-user', JSON.stringify(currentUser));
  },[currentUser]);

  const logout = () => {
    setCurrentUser({
      username: '',
      password: '',
      email: ''
    })
  }

  return (
    <BrowserRouter>
      
      <Switch>
        <Route exact path="/" render={() => <WelcomeComponent/>}/>
        <Route exact path='/home' render={() => <HomeComponent/>}  />
        <Route exact path='/profile' render={() => <ProfileComponent currentUser={currentUser} setCurrentUser={setCurrentUser} users={users} setUsers={setUsers} logout={logout}/>}  />
        <Route exact path="/login" render={() => <LoginComponent adminUser={adminUser} currentUser={currentUser} setCurrentUser={setCurrentUser} users={users}/>}/>
        <Route exact path="/register" render={() =><RegisterComponent users={users} setUsers={setUsers}/>}/>
        <Route exact path="/admin" render={() =><AdminComponent currentUser={currentUser} logout={logout} users={users} setUsers={setUsers} />}/>
        <Redirect to="/"/>
      </Switch>
    </BrowserRouter>
  );
}

export default App;
