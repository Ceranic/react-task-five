import React from 'react'
import { Link  } from 'react-router-dom';

function WelcomeComponent(props) {

    return (
    <div className="App">
        <div className="Welcome">
            <h1>Welcome to react task 5</h1>
        </div>
        <div className="welcomeButtons">
                <Link className="linkButton" to="/login">Login</Link>
                <Link className="linkButton" to="/register">Register</Link>
            </div>
    </div>
    )
}

export default WelcomeComponent
