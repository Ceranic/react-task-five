import React, {useState} from 'react'
import { withRouter, Redirect } from 'react-router-dom'

function LoginComponent(props) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [errors, setErrors] = useState([]);

    if(props.currentUser.username === 'admin'){
        return <Redirect to="/admin"/>

    }
    else if (props.currentUser.username !== ''){
        return <Redirect to="/home"/>

    }

    const goBack = () => {
        props.history.goBack();
    }

    const register = () => {
        props.history.push('/register')
    }

    const login = () => {
        if(email === props.adminUser.email && password === props.adminUser.password){
            props.setCurrentUser(props.adminUser);
        }
        else {
            const errors = [];
            if(password.length < 5) {
                errors.push('Password needs to be at least 5 characters long!');
            }
            if(!/\S+@\S+\.\S+/.test(email)){
                errors.push('Invalid Email format!');
            }
        
            setErrors(errors);

            if(errors.length === 0){
                const filterUsers = props.users.filter(x => x.email === email && x.password === password);
    
                console.log(filterUsers)
                if(filterUsers.length === 0){
                    setErrors(['User not found!'])
                }
                else {
                    props.setCurrentUser(filterUsers[0]);
                    props.history.push('/home')
                }
    
    
            }
        }
        

    }

    

    return (
        <div className="loginDiv">
            
            <div>
                <button className="loginButtonBack" onClick={goBack}>Back</button>
                <h1 className="loginHeader">Login</h1>
            </div>
            
            <div className="LoginInputsField">
            {errors.length > 0 ? (
                        <div className="ProfileErrors" >
                        {errors.map((x,y) => <p key={y}>{x}</p>)}
                    </div>
                    ):''}
            <div >
                <input className="loginInputs" type="email" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)}></input>
                <input className="loginInputs" type="password" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)}></input>
            </div>
            <div className="loginAndRegisterDiv">
                <button className="loginLoginButton" onClick={login}>Login</button>
                <button className="loginRegisterButton" onClick={register}>Register</button>
            </div>
            </div>
        </div>
    )
}

export default withRouter(LoginComponent)
