import React, { useState } from 'react'
import {withRouter} from 'react-router-dom'
function RegisterComponent(props) {

    const [email, setEmail] = useState('');
    const [password, setPassword] = useState('');
    const [username, setUsername] = useState('');
    const [repeatPassword, setRepeatPassword] = useState('');
    const [errors, setErrors] = useState([]);

    console.log(email); 
    // console.log(password);
    // console.log(username);

    const submit = () => {
        const errors = [];
        if(password.length < 5) {
            errors.push('Password needs to be at least 5 characters long!');
        }
        if(!/\S+@\S+\.\S+/.test(email)){
            errors.push('Invalid Email format!');
        }
        if(password !== repeatPassword){
            errors.push(' Password do not match!')
        }
        if(username.length < 8){
            errors.push(' Username is not valid!')
        }
    
        if(errors.length === 0){
            props.setUsers([...props.users, {
                username: username,
                password: password,
                email: email
            }]);
            props.history.push('/login');
        } else {
            setErrors(errors);
        }

    }



    return (
        <div>
            <div>
                <div className="registerForm">
                    <h1 className="registerHeader">Registration field</h1>
                    {errors.length > 0 ? (
                        <div className="ProfileErrors" >
                        {errors.map((x,y) => <p key={y}>{x}</p>)}
                    </div>
                    ):''}
                    <input className="registerEmail" type="email" placeholder="Enter your email" value={email} onChange={(e) => setEmail(e.target.value)}></input>
                    <input className="registerUsername" type="text" placeholder="Enter your username" value={username} onChange={(e) => setUsername(e.target.value)}></input>
                    <input className="registerPassword" type="password" placeholder="Enter your password" value={password} onChange={(e) => setPassword(e.target.value)}></input>
                    <input className="registerPasswordValidation" type="password" placeholder="Repeat your password" value={repeatPassword} onChange={(e) => setRepeatPassword(e.target.value)}></input>
                    <button className="registerButtonSubmit" onClick={submit}>Submit</button>
                </div>
            </div>
        </div>
    )
}

export default withRouter(RegisterComponent)