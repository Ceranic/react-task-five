import React from 'react'
import {Redirect} from 'react-router-dom';
import ListItemComponent from './ListItemComponent';

function AdminComponent(props) {

    if(props.currentUser.username !== 'admin'){
        return <Redirect to='/login'/>
    }

    const removeUser = (id) => {
        props.setUsers(props.users.filter((x,y) => y !== id))
    }

    return (
        <div>
            <div className="AdminHeader">
            <h1 className="AdminAbout">This is admin interface!</h1>
            <button onClick={props.logout}>Logout</button>
            </div>
            <div>
                {props.users.map((x,y) => <ListItemComponent onClick={() => removeUser(y)} id={y} key={y} data={x}/>)}
            </div>
        </div>
    )
}

export default AdminComponent
