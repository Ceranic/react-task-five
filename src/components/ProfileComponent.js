import React, {useState,useEffect} from 'react'
import {Redirect} from 'react-router-dom'
import './ProfileComponent.css'

function ProfileComponent(props) {

    

    const [username, setUsername] = useState(props.currentUser.username);
    const [password, setPassword] = useState(props.currentUser.password);
    const [email, setEmail] = useState(props.currentUser.email);
    const [errors, setErrors] = useState([]);
    const [edit, setEdit] = useState(false);

    const updateInfo = () => {
        


        if(edit) {
            const errors = [];
            if(password.length < 5) {
                errors.push('Password needs to be at least 5 characters long!');
            }
            if(!/\S+@\S+\.\S+/.test(email)){
                errors.push('Invalid Email format!');
            }
            if(!/^[a-z]+$/.test(username) || username.length < 8){
                errors.push("Username must be a-z at least 8 characters long!");
            }
            if(errors.length === 0) {
                const changedUser = {
                    username: username,
                    password: password,
                    email: email
                };
                props.setUsers(props.users.map((x,y) => (
                    x.username === props.currentUser.username &&
                    x.password === props.currentUser.password &&
                    x.email === props.currentUser.email) ? changedUser : x));

                props.setCurrentUser(changedUser);
                setErrors([]);
                setEdit(false);
            } else {
                setErrors(errors);

            }
        } else {
            setEdit(true);
        }
    }

    if(props.currentUser.username === '') {
        return <Redirect to="/login"/>
    }
    return (
        <div className="ProfileMain">
            <button onClick={props.logout}>Logout</button>
            <div className="ProfilePicture">
                <img src="https://www.petmd.com/sites/default/files/Acute-Dog-Diarrhea-47066074.jpg"></img>
            </div>
            {errors.length > 0 ? (
                <div className="ProfileErrors" >
                {errors.map((x,y) => <p key={y}>{x}</p>)}
            </div>
            ):''}
            <input disabled={!edit} className="ProfileInput" placeholder="Email" value={email} onChange={(e) => setEmail(e.target.value)}></input>
            <input disabled={!edit} className="ProfileInput" placeholder="Username" value={username} onChange={(e) => setUsername(e.target.value)}></input>
            <input disabled={!edit} className="ProfileInput" placeholder="Password" value={password} onChange={(e) => setPassword(e.target.value)}></input>
            <button className="ProfileButton" onClick={updateInfo}>
                {edit ? 'Save':'Edit'}
            </button>
        </div>
    )
}

export default ProfileComponent
