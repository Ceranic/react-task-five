import React from 'react'
import './ListItemComponent.css';

function ListItemComponent(props) {

    console.log(props);
    
    

    return (
        <div className="ListItemsMain">
            <div className="ListItemsId">{props.id+1}</div> 
            <div className="ListItemsEmail">{props.data.email}</div>
            <div className="ListItemsUsername">{props.data.username}</div>
            <div className="ListItemsPassword">{props.data.password}</div>
            <button className="ListItemsButton" onClick={props.onClick}>Remove</button>
        </div>
    )
}

export default ListItemComponent
