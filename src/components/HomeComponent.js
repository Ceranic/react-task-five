import React from 'react'
import {Link} from 'react-router-dom'
import './HomeComponent.css';
function HomeComponent(props) {
    return (
        <div className="HomeContainer">
            <h1>Home</h1>
            <p>Welcome to our app</p>
           <Link to="/profile" className="homeButton" >Profile</Link>
        </div> 
    )
}

export default HomeComponent